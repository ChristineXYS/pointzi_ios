Pod::Spec.new do |s|
  s.name                = "pointzi"
  s.header_dir          = "StreetHawkCore"
  s.version             = "0.1.0"
  s.summary             = "Release pointizi."
  s.description         = <<-DESC
                            TBD, waiting for copying from official notes.
                            DESC
  s.homepage            = "http://www.streethawk.com"
  s.screenshots         = [ ]
  s.license             = 'LGPL'
  s.author              = { 'Supporter' => 'support@streethawk.com' }
  s.documentation_url   = 'http://streethawksdk.github.io/ios/'

  s.source              = { :git => 'https://bitbucket.org/ChristineXYS/pointzi_ios', :tag => s.version.to_s, :submodules => true }
  s.platform            = :ios, '8.0'
  s.requires_arc        = true
  
  s.xcconfig            = { 'OTHER_LDFLAGS' => '$(inherited) -lObjC', 
                            'OTHER_CFLAGS' => '$(inherited) -DNS_BLOCK_ASSERTIONS=1 -DNDEBUG'
                          }  
                          
  s.source_files        = 'Pointzi/**/*.{h,m}'    
  s.public_header_files = 'Pointzi/Headers/*.h'
  s.vendored_libraries 	= 'Pointzi/libPointzi.a'
  s.resource_bundles    = {'Pointzi' => ['Pointzi/Assets/**/*']}
  s.dependency            'streethawk/Feed'
  
end
