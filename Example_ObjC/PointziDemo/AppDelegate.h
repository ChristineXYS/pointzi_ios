//
//  AppDelegate.h
//  PointziDemo
//
//  Created by Michael Xie on 18/02/2017.
//  Copyright © 2017 StreetHawk. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

